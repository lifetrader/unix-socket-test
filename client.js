const ipc=require('node-ipc');

ipc.config.id = 'client';
ipc.config.retry= 1000;
ipc.config.socketRoot = '';
ipc.config.appspace = '';

var connect_to = process.argv[2] || 'vault';

var request_json = 
'{"id" : 1,"type": "6445463409e8650e2b0286894630fce02b4ebacfe3f390099e840c76dcdb2809","from_address": "0xf7419c14cba7756947D40D8c04cD3cE0791dD9ff","to_address": "0xAeE64bD346D0E26b6E207181eD6b16de0C09B8Bf","ammount": "1"}'+
'{"id" : 4,"type": "6445463409e8650e2b0286894630fce02b4ebacfe3f390099e840c76dcdb2809","from_address": "0xf7419c14cba7756947D40D8c04cD3cE0791dD9ff","to_address": "0xAeE64bD346D0E26b6E207181eD6b16de0C09B8Bf","ammount": "2"}'+
'{"id" : 7,"type": "6445463409e8650e2b0286894630fce02b4ebacfe3f390099e840c76dcdb2809","from_address": "0xf7419c14cba7756947D40D8c04cD3cE0791dD9ff","to_address": "0xAeE64bD346D0E26b6E207181eD6b16de0C09B8Bf","ammount": "3"}'+
'\n';

// var request_json = 
// '{"id" : 1,"type": "08f1c1345cdeed7d390682356b4ed3c87197f0fd0c415c27d92e704f452b1250","to_address": "0xf7419c14cba7756947D40D8c04cD3cE0791dD9ff","from_address": "0xAeE64bD346D0E26b6E207181eD6b16de0C09B8Bf","ammount": "1"}'+
// '{"id" : 4,"type": "08f1c1345cdeed7d390682356b4ed3c87197f0fd0c415c27d92e704f452b1250","to_address": "0xf7419c14cba7756947D40D8c04cD3cE0791dD9ff","from_address": "0xAeE64bD346D0E26b6E207181eD6b16de0C09B8Bf","ammount": "2"}'+
// '{"id" : 7,"type": "08f1c1345cdeed7d390682356b4ed3c87197f0fd0c415c27d92e704f452b1250","to_address": "0xf7419c14cba7756947D40D8c04cD3cE0791dD9ff","from_address": "0xAeE64bD346D0E26b6E207181eD6b16de0C09B8Bf","ammount": "3"}'+
// '\n';

// var request_json = 
// '{"id" : 1,"type": "74ffa5b4e73285a5ad175ad77be0a31eef00369d388ff78adadbd9122e7d7236","to_address": "0xf7419c14cba7756947D40D8c04cD3cE0791dD9ff","from_address": "0xeff80ecd50a674f77f399d3cb8d4adedc6c4c8f0","ammount": "1"}'+
// '{"id" : 4,"type": "74ffa5b4e73285a5ad175ad77be0a31eef00369d388ff78adadbd9122e7d7236","to_address": "0xf7419c14cba7756947D40D8c04cD3cE0791dD9ff","from_address": "0xeff80ecd50a674f77f399d3cb8d4adedc6c4c8f0","ammount": "2"}'+
// '{"id" : 7,"type": "74ffa5b4e73285a5ad175ad77be0a31eef00369d388ff78adadbd9122e7d7236","to_address": "0xf7419c14cba7756947D40D8c04cD3cE0791dD9ff","from_address": "0xeff80ecd50a674f77f399d3cb8d4adedc6c4c8f0","ammount": "3"}'+
// '\n';

ipc.connectTo(
    connect_to,
    function(){
        ipc.of[connect_to].on(
            'connect',
            function(){
                console.log("connected");
                ipc.of[connect_to].emit(
                    'app.message',
                    request_json
                );
            }
        );
        ipc.of[connect_to].on(
            'disconnect',
            function(){
                ipc.log('client disconnected from vault');
            }
        );
        ipc.of[connect_to].on(
            'message',
            function(data){
                ipc.log('response:');
                ipc.log(data.message);
            }
        );

    }
);