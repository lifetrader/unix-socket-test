const Web3 = require('web3');
var Tx = require('ethereumjs-tx');
const file_functions = require('./file_functions');

const JSON = require("JSON");

const ropsten_url = 'https://ropsten.infura.io/TjHaheHFI2wmX9PA3IzE';

module.exports = {

  processTransaction : async function(request_json){

      var response = '';
      request_json = request_json.replace(/}/g,'}|').split('|');
      var i = 0;

      while(request_json.length != 0){

        var temp_json = request_json.shift();

        if(temp_json == '\n'){
          return Promise.resolve(response);
        }
        else{
          
          temp_json = JSON.parse(temp_json);
          temp_json.sender_private_key = await file_functions.getSenderPrivateKey(temp_json.from_address);

          if(temp_json.sender_private_key === undefined){
            console.log("private key of sender not found, carrying on to next transaction");
            continue;
          }

          await this.createAndSignTransaction(temp_json,i++).then(function(result_json){
              response += result_json;
          });

        }

      }//end of while(request_json.length != 0)



  },
  createAndSignTransaction : async function(objData,index){

    return new Promise((resolve,reject)=>{ //begin promise
      const web3 = new Web3();

      web3.setProvider(new web3.providers.HttpProvider(ropsten_url));
        // web3.setProvider(new web3.providers.HttpProvider('https://ropsten.infura.io/TjHaheHFI2wmX9PA3IzE'));

        // var privateKey = new Buffer('6445463409e8650e2b0286894630fce02b4ebacfe3f390099e840c76dcdb2809', 'hex') //D5K353R
        // var from_address = '0xf7419c14cba7756947D40D8c04cD3cE0791dD9ff';//D5K353R
        // var to_address = '0xAeE64bD346D0E26b6E207181eD6b16de0C09B8Bf';//SDRVLC

        
        var privateKey = new Buffer(objData.sender_private_key || objData.type, 'hex'); //D5K353R
        var from_address = objData.from_address; //D5K353R
        var to_address = objData.to_address; //SDRVLC

        //private keys
        // 6445463409e8650e2b0286894630fce02b4ebacfe3f390099e840c76dcdb2809 --- D5K353R
        // 08f1c1345cdeed7d390682356b4ed3c87197f0fd0c415c27d92e704f452b1250 --- SDRVLC

        // var estimate = web3.eth.estimateGas({
        //     to: "0xAeE64bD346D0E26b6E207181eD6b16de0C09B8Bf", 
        //     data: "0x"
        // });

        var estimateGas = web3.eth.estimateGas({
          to: to_address 
        });

        // var estimateLimit = web3.eth.estimateGas({
        //   to: to_address 
        // }) * web3.toWei(objData.ammount,'wei');

        // var current_nonce = web3.eth.getTransactionCount('0xf7419c14cba7756947D40D8c04cD3cE0791dD9ff');

        // var gasPrice = 2*10**10;

        var gasPrice = 2*10**10 * objData.ammount;

        // console.log(gasPrice);
        // console.log(estimateGas);
        // console.log(web3.toWei(parseInt(1)));
        var attoethForGas = gasPrice * estimateGas;
        // console.log(web3.toWei(parseInt(1)) - attoethForGas);

        var current_nonce = web3.eth.getTransactionCount(from_address) + index;

        var rawTx = {
          nonce: current_nonce,
          gas: estimateGas, 
          gasPrice: gasPrice, 
          // gasLimit: estimateLimit,
          from: from_address,//D5K353R
          to: to_address, //SDRVLC
          // value: web3.toWei(objData.ammount,'Ether')
          // value: web3.toWei(parseInt(1)) - attoethForGas
          value: web3.toWei(parseInt(objData.ammount)) - attoethForGas
        }

        //transaction block------------------------------------------------------------------
        var tx = new Tx(rawTx);
        tx.sign(privateKey);

        var serializedTx = tx.serialize();

          web3.eth.sendRawTransaction('0x' + serializedTx.toString('hex'), function(err, hash) {

            if(err){
              console.log(objData.id,": error ",err);
               json_string = JSON.stringify({
                  id : objData.id,
                  tx : err
                });

              resolve(json_string);
            }
            else if(!err){

            // var receipt = web3.eth.getTransactionReceipt('0x3fe3361ded17f60a3ad0afd5727e863300e6ec3e7e727d1cc8e514784afd2929');
              // var receipt = web3.eth.getTransactionReceipt(hash,function(err,detail){
              //   console.log(detail);
              // });

              json_string = JSON.stringify({
                id : objData.id,
                tx : hash
              });

              resolve(json_string);
            }

          });
      })//promise end
      
      
      //transaction block end------------------------------------------------------------------

    },

  }

