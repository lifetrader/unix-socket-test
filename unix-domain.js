if(!process.argv[2]){
	console.log("please add first parameter for unix-domain name");
	return;
}

const ipc=require('node-ipc');
const web3_functions = require('./web3_functions');

ipc.config.id = process.argv[2] || 'vault';
ipc.config.retry= 1500;
ipc.config.socketRoot = '';
ipc.config.appspace = '';

ipc.serve(
    function(){
        ipc.server.on(
            'app.message',
            function(data,socket){
            	console.log('data : ',data )

            	var response = '';
				web3_functions.processTransaction(data).then(function(res){
				    response = res;

				    console.log('final',response)
				    ipc.server.emit(
	                    socket,
	                    'message',
	                    response
	                );

				})
				.catch(function(err){
				  console.log("there's an error: ",err)
				})

                
            }
        );
    }
);

ipc.server.start();