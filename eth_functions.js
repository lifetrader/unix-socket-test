const request = require("request");
const JSON = require("JSON");
const fs = require("fs");


const url = 'https://ropsten.infura.io/TjHaheHFI2wmX9PA3IzE';

module.exports = {
  eth_blockNumber : function(result){

      var options = { 
         method: 'POST',
         url: url,
         headers: 
          { 
            'Accept': 'application/json',
            'Content-Type': 'Application/json' 
          },
        // body: '{\n\t"jsonrpc":"2.0",\n\t"method":"eth_blockNumber",\n\t"params":[],\n\t"id":1\n}' 
        body: JSON.stringify({
                "jsonrpc":"2.0",
                "method":"eth_blockNumber",
                // "method":"eth_getTransactionCount",
                // "params":['0x407d73d8a49eeb85d32cf465507dd71d507100c1','latest'],
                "params":[],
                "id":1
        })
        // body: data

      };

      // var callback = function (error, response, body) {
      //   if (error) throw new Error(error);

      //   // console.log(body);
      //   return body;
      // };

      return new Promise((resolve,reject) => {
        request(options, function (error, response, body) {
          if (error) return error;

          resolve(body);
        });
      })
  },

  eth_sendTransaction : function(data){

      // var data = {
      //     "jsonrpc":"2.0",
      //     "method":"eth_sendRawTransaction",
      //     "params":["0xd46e8dd67c5d32be8d46e8dd67c5d32be8058bb8eb970870f072445675058bb8eb970870f072445675"],
      //     "id":1
      // };

      var options = { 
        method: 'POST',
        url: url,
        headers: 
            {
             "Accept": 'application/json',
             'Content-Type': 'Application-json' 
            },
        body:data
      };


      return new Promise((resolve,reject) => {
        request(options, function (error, response, body) {
          if (error) resolve(error);
          
          resolve(body);
        });
      });
  }

}

